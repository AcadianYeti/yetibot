const axios = require('axios')
const discord = require('discord.io')
const logger = require('winston')
const fs = require('fs')
const auth = require("./auth.json")

const SCORE_DISTRIBUTION = [
    25,
    15,
    10,
    7,
    4,
    3,
    2,
    2
]

logger.remove(logger.transports.Console)
logger.add(new logger.transports.Console, {
    colorize: true
})
logger.level = 'debug'

let bot = new discord.Client({
    token: auth.token,
    autorun: true
})


bot.on('ready', function(evt) {
    logger.info('Connected')
    logger.info('Logged in as: ')
    logger.info(bot.username + bot.id)
})

bot.on('message', function(user, userID, channelID, message, evt) {
    if(message.charAt(0) === '!') {
        // Divide into commands
        let cmds = message.split(' ')
        let game, playState, bracketLink
        switch(cmds[0]) {
            case '!bracket-entry': 
                if(cmds.length != 4) {
                    bot.sendMessage({
                        to:channelID,
                        message: "Incorrect amount of paramaters, need game, playstate and bracket link"
                    })   
                }
                game = cmds[1].toLowerCase()
                playState = cmds[2].toLowerCase()
                bracketLink = cmds[3]

                parseBracket(bracketLink).then((result) => {
                    updateBracketFile(game, result, playState)
                })
                break;
            case '!leaderboard':
                if(cmds.length < 2) {
                    bot.sendMessage({
                        to:channelID,
                        message: "Incorrect amount of paramaters"
                    })   
                    return
                }
                game = cmds[1]
                let results = getLeaderboard(game)
                printLeaderboard(results, channelID, game)



                
            default:
                break;
        }
    }
})

const printLeaderboard = (results, channelID,  game, player = 'all', playMode = 'any') => {
    let stringToSend = "```====================================================\n" +
    "\t\t\t\tOffline Leaderboard\n" +
    "====================================================\n" + 
    "\tRank\t|\tName\t|\tWins\t|\tPoints" + "\n" +
    "----------------------------------------------------\n"


    let currLeaderboard, ranks = '', name ='', wins ='', points =''
    if(results.offline) {
        currLeaderboard = results.offline.leaderboard
        if(currLeaderboard) {
            currLeaderboard.sort((a, b) => {
                return a.data.points - b.data.points
            })
            for(let i = 0; i < currLeaderboard.length; i++) {
                let element = currLeaderboard[i]

                stringToSend += "\t" + (i+1) + "\t|\t" + element.name + "\t|\t" + element.data.wins + "\t|\t" + element.data.points + "\n"
                stringToSend += "----------------------------------------------------\n"
            }
        }
    }
    let leaderboardEmbed = new discord.MessageEmbed()
        .setTitle(game + "Offline Leaderboard")
        .addFields(
            { name: "Rank", value: "1"},
            { name: "Name", value: "Long ass name testing"},
            { name: "Wins", value: "10" },
            { name: "Points", value: "100" }
        )

    /*
    stringToSend += "====================================================\n" +
    "\t\t\tOnline Leaderboard\n" +
    "====================================================\n" + 
    "\tRank\t|\tName\t|\tWins\t|\tPoints" + "\n" +
    "----------------------------------------------------\n"
    if(results.online) {
        currLeaderboard = results.online.leaderboard
        if(currLeaderboard) {
            currLeaderboard.sort((a, b) => {
                return a.data.points - b.data.points
            })
            for(let i = 0; i < currLeaderboard.length; i++) {
                let element = currLeaderboard[i]
                stringToSend += "\t" + (i+1) + "\t|\t" + element.name + "\t|\t" + element.data.wins + "\t|\t" + element.data.points + "\n"
                stringToSend += "----------------------------------------------------\n"
            }
        }
    }
    stringToSend += "```"
    */
    bot.sendMessage({
        to:channelID,
        message: stringToSend
    })   
}

const extractName = (line) => {
    let endIndex = line.indexOf("</a>")
    let startIndex = endIndex 
    let found = false
    while(startIndex > 0 && !found) {
        if(line.charAt(startIndex) === '>') {
            return line.substring(startIndex + 1, endIndex)
            found = true
        }
        else {
            startIndex--
        }
            
    }
}

getLeaderboard =  (game) => {
    let finalResults = {
        online: [],
        offline: []
    }
    let results
    try {
        let offlineLeaderboard = "leaderboards/" + game + "-offline-leaderboard.json"
        if(fs.existsSync(offlineLeaderboard)) {
            console.log("Found offline leaderboards for " + game)
            results = fs.readFileSync(offlineLeaderboard)
            finalResults.offline = JSON.parse(results)
        }

        let onlineLeaderboard = "leaderboards/" + game + "-online-leaderboard.json"
        if(fs.existsSync(onlineLeaderboard)) {
            console.log("Found online leaderboards for " + game)
            results = fs.readFileSync(onlineLeaderboard)
            finalResults.online = JSON.parse(results)
        }
        
    }
    catch (e) {
        console.log("error occured parsing files for bracket " + e.message)
    }
    return finalResults
}

updateBracketFile = (game, parsedBracket, playState) => {

    try {
        let leaderboardFile = "leaderboards/" + game + "-" + playState + "-leaderboard.json"
        if(fs.existsSync(leaderboardFile)) {
            console.log("Found existing leaderboard file for " + game)
            // Get current data
            fs.readFile(leaderboardFile, function(err, data) {
                if(err) {
                    console.log("error occured reading file " + leaderboardFile)
                    return
                }
                let currentResults = JSON.parse(data)
                for(let i = 0; i < parsedBracket.leaderboard.length; i++) {
                    let hasName = currentResults.leaderboard.find((element) => {
                        return element.name === parsedBracket.leaderboard[i].name
                    })
                    if(hasName) {
                        // Update value
                        hasName.data.wins += parsedBracket.leaderboard[i].data.wins
                        hasName.data.losses += parsedBracket.leaderboard[i].data.losses
                        hasName.data.points += parsedBracket.leaderboard[i].data.points
                    }
                    else {
                        // New value add it
                        currentResults.leaderboard.push(parsedBracket.leaderboard[i])
                    }
                }
                fs.writeFile(leaderboardFile, JSON.stringify(currentResults), function(err) {
                    if(err) {
                        console.log("Error writing to file " + err.message)
                        return
                    }
                })
            })
        }
        else {
            console.log("No leaderboard found for " + game + ", creating one")
            fs.writeFile(leaderboardFile, JSON.stringify(parsedBracket), function(err) {
                if(err) {
                    console.log("error occured when writing to file" + err)
                    return
                }

            })
        }
    } 
    catch (e) {
        console.log("error occured with " + e)
    }
}



parseBracket = async (bracket) => {
    let users = []
    let results = []

    await axios
    .get(bracket)
    .then(res=> {
        let resultString = res.data
        let startIndex = resultString.indexOf("Final Results")
        let endIndex = resultString.indexOf("Full Bracket")
        resultString = resultString.substring(startIndex, endIndex)
        let splitStrings = resultString.split('\n')


        let foundUser = false
        let foundWins = false
        let currIndex = 0
        splitStrings.forEach(line => {
            if(foundWins) {
                if(line.charAt(0) !== '-') {
                    results[currIndex].losses = parseInt(line.charAt(0))
                    results[currIndex].points = currIndex >= SCORE_DISTRIBUTION.length ? 1 : SCORE_DISTRIBUTION[currIndex]
                    currIndex++ 
                    foundUser = false
                    foundWins = false
                }
            }
            else if(foundUser) {
                if(line.charAt(0) !== '<') {
                    results.push({
                        wins: parseInt(line.charAt(0)),
                        losses: ""})
                    foundWins = true
                }
                    
            }
            else if(line.includes("h5")) {
                users.push(extractName(line))
                foundUser = true
            }
                
        })

    })
    .catch(error =>{ 
        console.log("error making request")
        console.log(error)
    })
    let resultObj = {
        leaderboard: []
    }

    for(let i = 0; i < users.length; i++) {
        resultObj.leaderboard.push({
            name: users[i].toLowerCase(),
            data: results[i]
        })
    }
    /*
    bot.sendMessage({
        to: channelID,
        message: resultObj
    })
    */

    return resultObj
}
