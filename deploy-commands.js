const { SlashCommandBuilder } = require('@discordjs/builders')
const { REST } = require('@discordjs/rest')
const { Routes } = require('discord-api-types/v9')
const { clientId, guildId, token } = require('./auth.json')

const commands = [
    new SlashCommandBuilder()
        .setName('set-bracket-link')
        .setDescription("Sets bracket of particular game")
        .addStringOption((option) => 
            option.setName('game')
                .setDescription("Bracket Game")
                .setRequired(true)
        )
        .addStringOption((option) =>
            option.setName('bracket_link')
                .setDescription("Link to bracket")
                .setRequired(true)
        ),
    new SlashCommandBuilder()
        .setName('bracket')
        .setDescription('Prints brackets for tonights halifax local'),
    new SlashCommandBuilder()
        .setName('leaderboard')
        .setDescription("Fetches current leaderboard!")
        .addStringOption((option) => 
            option.setName('game')
                .setDescription("Game played")
                .setRequired(true)
        )
        .addStringOption((option) => 
            option.setName('environment')
                .setDescription("Online/Offline")
                .setRequired(true)
                .addChoices({
                    name: 'Online',
                    value: 'online'
                }, {
                    name: "Offline",
                    value: "offline"
                })
        )
        .addStringOption((option) => 
            option.setName('me')
                .setDescription("My Username")
        ),
    new SlashCommandBuilder()
        .setName('upload-bracket')
        .setDescription("Uploads challonge leaderboard")
        .addStringOption((option) => 
            option.setName('game')
                .setDescription("Game played")
                .setRequired(true)
        )
        .addStringOption((option) => 
            option.setName('environment')
                .setDescription("Online/Offline")
                .setRequired(true)
                .addChoices({
                    name: 'Online',
                    value: 'online'
                }, {
                    name: "Offline",
                    value: "offline"
                })
        )
        .addStringOption((option) => 
            option.setName('bracket-link')
            .setDescription("Link to challonge bracket")
            .setRequired(true)
        )
        .addStringOption((option) =>
            option.setName("challonge-name")
            .setDescription("Challonge name for bracket finding")
            .setRequired(true)
        )
        .addStringOption((option) => 
            option.setName("api-key")
            .setDescription("API key for tourney")
        ),
    new SlashCommandBuilder()
        .setName('leaderboard-long')
        .setDescription("fetches full leaderboard")
        .addStringOption((option) => 
            option.setName('game')
                .setDescription("Game played")
                .setRequired(true)
        )
        .addStringOption((option) => 
            option.setName('environment')
                .setDescription("Online/Offline")
                .setRequired(true)
                .addChoices({
                    name: 'Online',
                    value: 'online'
                }, {
                    name: "Offline",
                    value: "offline"
                })
        )
        .addStringOption((option) => 
            option.setName('me')
                .setDescription("My Username")
        ),
        
].map(commands => commands.toJSON())

const rest = new REST({version: '9'}).setToken(token)

rest.put(Routes.applicationGuildCommands(clientId, guildId), {body: commands})
    .then(() => console.log('Successfully registered commands'))
    .catch(console.error)