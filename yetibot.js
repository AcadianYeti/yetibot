const discord = require('discord.js')
const { token, challongeAPI } = require('./auth.json')
const axios = require('axios')
const fs = require('fs')
const aliasHelper = require('./aliases.js')

const MOD_ROLE_ID = '382301983740002336'
const ADMIN_ROLE_ID = '382303215850487828'
const ONE_WEEK = 604000000

const SCORE_DISTRIBUTION = [
    {
        rank: 1,
        points: 25
    },
    {
        rank: 2,
        points: 15
    },
    {
        rank: 3,
        points: 10
    },
    {
        rank: 4,
        points: 7
    },
    {
        rank: 5,
        points: 5
    },
    {
        rank: 7,
        points: 3
    },
    {
        rank: 9,
        points: 2
    }
]

const client = new discord.Client({intents: [discord.Intents.FLAGS.GUILDS]})

client.once('ready', () => {
    console.log("Ready!")
})


client.on('interactionCreate', async interaction =>{
    if(!interaction.isCommand()) 
        return
    try {
        const { commandName } = interaction
        switch(commandName){
            case "leaderboard": 
                parseLeaderboardCommand(interaction)
                break;
            case "upload-bracket":
                if(interaction.member._roles.includes(MOD_ROLE_ID) || interaction.member_roles.includes(ADMIN_ROLE_ID))
                    parseUploadBracketCommand(interaction)
                break;
            case "bracket": 
                parsePrintBracket(interaction)
                break;
            case "set-bracket-link": 
                if(interaction.member._roles.includes(MOD_ROLE_ID) || interaction.member._roles.includes(ADMIN_ROLE_ID))
                    parseSetBracketLink(interaction)
                break;
            case "bracket":
                printBrackets(interaction)
                break;
            case "leaderboard-long":
                parseLeaderboardCommand(interaction, true)
                break;
        }
    }
    catch(e) {
        console.log("Something went wrong with command " + interaction)
    }

})

const parsePrintBracket = (interaction) => {
    let bracketFile = "brackets.json"
    try {
        if(fs.existsSync(bracketFile)) {
            let results = fs.readFileSync(bracketFile)
            results = JSON.parse(results)
            let formattedText = ""
            Object.keys(results).forEach((key) => {
                if((Date.now() - results[key].time) < ONE_WEEK) {
                    formattedText += `[${key.toUpperCase()}](${results[key].link})\n`
                }
            }) 
            if(formattedText.length === 0) {
                formattedText = "Unabled to find any recent brackets!"
            }
            interaction.reply(formattedText)
        }
        else {
            interaction.reply("No brackets currently on!")
        }
    }
    catch(e) {
        console.log("Failed to print brackets")
        console.log(e)
    }
}

const parseSetBracketLink = (interaction) => {
    let game = interaction.options.getString("game").toLowerCase()
    let bracketLink = interaction.options.getString("bracket_link")

    let bracketFile = "brackets.json"
    try {
        if(fs.existsSync(bracketFile)) {
            let results = fs.readFileSync(bracketFile)
            results = JSON.parse(results)
            results[game] = {
                link: bracketLink,
                time: Date.now()
            }

            fs.writeFile(bracketFile, JSON.stringify(results), function(err) {
                if(err) {
                    console.log("Error occured when writing to file " + err)
                    return
                }
            })
            interaction.reply("Successfully set bracket link for " + game)
        }
        else {
            let newObj = {}
            newObj[game] = {
                link: bracketLink,
                time: Date.now()
            }
                
            fs.writeFile(bracketFile, JSON.stringify(newObj), function(err) {
                if(err) {
                    console.log("error occured when writing to file" + err)
                    return
                }
    
            })
            interaction.reply("Successfully set bracket link for " + game)
        }
    }
    catch(e) {
        console.log("Failed to parse set bracket link")
    }
}

const parseLeaderboardCommand = (interaction, printAll = false) => {
    let game = interaction.options.getString("game").toLowerCase()
    let env = interaction.options.getString("environment")
    let me = interaction.options.getString("me")
    if(me) {
        let converted = aliasHelper.convertName(me)
        if(converted != undefined)
            me = converted
    }
    let results = getLeaderboard(game)
    if(env === 'online' && results.online.length === 0) {
        interaction.reply("Couldn't find an online bracket for that game!")
        return
    }
    else if(env === "offline" && results.offline.length === 0) {
        interaction.reply("Couldn't find an offline bracket for that game!")
        return
    }
    printLeaderboard(results, interaction, game, me, env, printAll)
}

const parseUploadBracketCommand = async (interaction) => {
    let game = interaction.options.getString("game").toLowerCase()
    let env = interaction.options.getString("environment")
    let bracket = interaction.options.getString("bracket-link")
    let apiKey = interaction.options.getString("api-key")
    let userName = interaction.options.getString("challonge-name")
    let foundData = false
    await parseBracket(bracket, apiKey, userName).then((result) => {
        updateBracketFile(game, result, env)
        addBracketToRecords(bracket, game, env)
        if(result.leaderboard.length !== 0)
            foundData = true
    }).catch(error => {
        interaction.reply("Error thrown when parsing bracket")
        console.log(error)
        return
    })
    if(!foundData)
	    interaction.reply("Didn't find any data for that leaderboard, consider checking api key, bracket link and name")
    else
    	interaction.reply("Successfully updated bracket!")
}

const printLeaderboard = (results, interaction,  game, includeAlwaysName, mode = 'offline', printAll = false) => {
    let currLeaderboard, ranks = '', name ='', points ='', foundInclude, leaderboardEmbed
    if(mode.toLowerCase() === 'offline') {
        currLeaderboard = results.offline.leaderboard
    }
    else {
        currLeaderboard = results.online.leaderboard
    }

    if(currLeaderboard) {
        currLeaderboard.sort((a, b) => {
            return b.data.points - a.data.points
        })
        for(let i = 0; i < currLeaderboard.length && (i < 15 || printAll); i++) {
            let element = currLeaderboard[i]
            ranks += (i + 1) + '\n'
            name += element.name + '\n'
            if(element.name.toLowerCase() === includeAlwaysName) {
                foundInclude = true
            }
            points += element.data.points + '\n'
        }
    }

    if(!foundInclude) {
        for(let i = 0; i < currLeaderboard.length; i++) {
            if(currLeaderboard[i].name.toLowerCase() === includeAlwaysName) {
                let element = currLeaderboard[i]
                ranks += '\n' + (i + 1) + '\n'
                name += '\n' + element.name + '\n'
                points += '\n' + element.data.points + '\n'
                i = currLeaderboard.length
            }
        }
    }
    leaderboardEmbed = new discord.MessageEmbed()
        .setTitle(game.toUpperCase() + " Offline Leaderboard")
        .setThumbnail("https://cdn.discordapp.com/attachments/629651982637858816/963586075069136926/received_2545381825691515.png")
        .addFields(
            { name: "Rank", value: ranks, inline: true},
            { name: "Name", value: name, inline: true},
            { name: "Points", value: points, inline: true },
        )
    /*
    stringToSend += "====================================================\n" +
    "\t\t\tOnline Leaderboard\n" +
    "====================================================\n" + 
    "\tRank\t|\tName\t|\tWins\t|\tPoints" + "\n" +
    "----------------------------------------------------\n"
    if(results.online) {
        currLeaderboard = results.online.leaderboard
        if(currLeaderboard) {
            currLeaderboard.sort((a, b) => {
                return a.data.points - b.data.points
            })
            for(let i = 0; i < currLeaderboard.length; i++) {
                let element = currLeaderboard[i]
                stringToSend += "\t" + (i+1) + "\t|\t" + element.name + "\t|\t" + element.data.wins + "\t|\t" + element.data.points + "\n"
                stringToSend += "----------------------------------------------------\n"
            }
        }
    }
    stringToSend += "```"
    */
    interaction.reply({embeds: [leaderboardEmbed]})
    
}

getLeaderboard =  (game) => {
    let finalResults = {
        online: [],
        offline: []
    }
    let results
    try {
        let offlineLeaderboard = "leaderboards/" + game + "-offline-leaderboard.json"
        if(fs.existsSync(offlineLeaderboard)) {
            console.log("Found offline leaderboards for " + game)
            results = fs.readFileSync(offlineLeaderboard)
            finalResults.offline = JSON.parse(results)
        }

        let onlineLeaderboard = "leaderboards/" + game + "-online-leaderboard.json"
        if(fs.existsSync(onlineLeaderboard)) {
            console.log("Found online leaderboards for " + game)
            results = fs.readFileSync(onlineLeaderboard)
            finalResults.online = JSON.parse(results)
        }
        
    }
    catch (e) {
        console.log("error occured parsing files for bracket " + e.message)
    }
    return finalResults
}

addBracketToRecords = (bracketUrl, game, playState) => {
    let bracketRecord = "uploadedBrackets/" + game + "-" + playState + "-uploaded-brackets.txt"
    if(fs.existsSync(bracketRecord)) {
        console.log("Found existing bracket record for " + game)
        fs.readFile(bracketRecord, 'utf-8', function(err, data) {
            if(err) {
                console.log("Error occured reading file" + bracketRecord)
                return
            }

            let newEntry = data + bracketUrl + "\n"
            fs.writeFile(bracketRecord, newEntry, function(err) {
                if(err) {
                    console.log("Error writing file to " + bracketRecord)
                    return
                }
            })
        })
    }
    else {
        let initialEntry = bracketUrl + "\n"
        fs.writeFile(bracketRecord, initialEntry, function(err) {
            if(err) {
                console.log("Error writing file to " + bracketRecord)
                return
            }
        })
    }
}

updateBracketFile = (game, parsedBracket, playState) => {

    try {
        let leaderboardFile = "leaderboards/" + game + "-" + playState + "-leaderboard.json"
        if(fs.existsSync(leaderboardFile)) {
            console.log("Found existing leaderboard file for " + game)
            // Get current data
            fs.readFile(leaderboardFile, function(err, data) {
                if(err) {
                    console.log("error occured reading file " + leaderboardFile)
                    return
                }
                let currentResults = JSON.parse(data)
                for(let i = 0; i < parsedBracket.leaderboard.length; i++) {
                    let hasName = currentResults.leaderboard.find((element) => {

                        return element.name.toLowerCase() === parsedBracket.leaderboard[i].name.toLowerCase()
                    })
                    if(hasName) {
                        // Update value
                        hasName.data.wins += parsedBracket.leaderboard[i].data.wins
                        hasName.data.losses += parsedBracket.leaderboard[i].data.losses
                        hasName.data.points += parsedBracket.leaderboard[i].data.points
                    }
                    else {
                        // New value add it
                        currentResults.leaderboard.push(parsedBracket.leaderboard[i])
                    }
                }
                fs.writeFile(leaderboardFile, JSON.stringify(currentResults), function(err) {
                    if(err) {
                        console.log("Error writing to file " + err.message)
                        return
                    }
                })
            })
        }
        else {
            console.log("No leaderboard found for " + game + ", creating one")
            fs.writeFile(leaderboardFile, JSON.stringify(parsedBracket), function(err) {
                if(err) {
                    console.log("error occured when writing to file" + err)
                    return
                }

            })
        }
    } 
    catch (e) {
        console.log("error occured with " + e)
    }
}


parseBracket = async (bracket, apiKey, userName) => {
    let users = []
    let results = []
    let apiToUse = apiKey ? apiKey : challongeAPI

    await axios
    .get(`https://${userName}:${apiToUse}@api.challonge.com/v1/tournaments.json`)
    .then(res=> {
        let resultTourney = res.data.find((element) =>  {
            return element.tournament.full_challonge_url === bracket
        })

        if(!resultTourney)
            return
        
        return axios.get(`https://${userName}:${apiToUse}@api.challonge.com/v1/tournaments/${resultTourney.tournament.id}/participants.json`)
        .then(res => {
            let participants = res.data
            for(let i = 0; i < participants.length; i++) {
                let participant = participants[i].participant
                let convertedName = aliasHelper.convertName(participant.name.toLowerCase())
                if(convertedName === undefined)
                    convertedName = participant.name
                users.push(convertedName)
                let pointsEarned = SCORE_DISTRIBUTION.find((element) => {
                    return element.rank === participant.final_rank
                })

                results.push({
                    points: pointsEarned ? pointsEarned.points : 1
                })
            }
        })
        .catch(error => {
            console.log(error)
            return
        }) 
    })
    .catch(error =>{ 
        console.log("error making request")
        console.log(error)
        return
    })
    let resultObj = {
        leaderboard: []
    }
    for(let i = 0; i < users.length; i++) {
        resultObj.leaderboard.push({
            name: users[i],
            data: results[i]
        })
    }

    
        /*
        axios.get(`https://ayeti:${challongeAPI}@api.challonge.com/v1/tournaments/${resultTourney.tournament.id}/matches.json`)
            .then(res => {
                let matches = res.data
        })
        .catch(error => {
            console.log(error)
        })
        //parseMatches(resultTourney.tournament)

        let startIndex = resultString.indexOf("Final Results")
        let endIndex = resultString.indexOf("Full Bracket")
        resultString = resultString.substring(startIndex, endIndex)
        let splitStrings = resultString.split('\n')


        let foundUser = false
        let foundWins = false
        let currIndex = 0
        splitStrings.forEach(line => {
            if(foundWins) {
                if(line.charAt(0) !== '-') {
                    results[currIndex].losses = parseInt(line.charAt(0))
                    results[currIndex].points = currIndex >= SCORE_DISTRIBUTION.length ? 1 : SCORE_DISTRIBUTION[currIndex]
                    currIndex++ 
                    foundUser = false
                    foundWins = false
                }
            }
            else if(foundUser) {
                if(line.charAt(0) !== '<') {
                    results.push({
                        wins: parseInt(line.charAt(0)),
                        losses: ""})
                    foundWins = true
                }
                    
            }
            else if(line.includes("h5")) {
                users.push(extractName(line))
                foundUser = true
            }
                
        })
        */

    /*
    bot.sendMessage({
        to: channelID,
        message: resultObj
    })
    */

    return resultObj
}

parseMatches = async(tournament) => {
    await axios
    .get(`https://ayeti:${challongeAPI}@api.challonge.com/v1/tournaments/${tournament.id}/matches.json`)
    .then(res => {
        console.log("Got results of this ping")
        console.log(res)
    })
    .catch(error => {
        console.log(error)
    })
}



client.login(token)
